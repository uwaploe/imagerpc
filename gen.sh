#!/usr/bin/env bash
#
# Generate Python modules from protobuf file
#
python3 -m grpc_tools.protoc \
       -I./protos \
       --python_out=./imagesvc \
       --grpc_python_out=./imagesvc \
       --mypy_out=./imagesvc \
       ./protos/imagesvc.proto
# Fix import statement in gRPC module
cd imagesvc && sed -i '' 's/^\(import.*pb2\)/from . \1/g' imagesvc_pb2_grpc.py
