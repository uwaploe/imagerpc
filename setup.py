from setuptools import setup


setup(name='imagesvc',
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      description='gRPC image acquisition server for the inVADER project',
      url='https://bitbucket.org/uwaploe/imagerpc',
      author='Michael Kenney',
      author_email='mikek@apl.uw.edu',
      license='GPL',
      packages=['imagesvc'],
      install_requires=[
          'grpcio',
          'tifffile',
          'grpcio-tools',
          'grpcio-reflection',
      ],
      entry_points = {
          'console_scripts': ['iaserver=imagesvc.cli:main',
                              'iatest=imagesvc.testcli:main'],
      },
      zip_safe=False)
