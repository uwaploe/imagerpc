# inVADER Image Acquisition Service

This Python (v3) package implements a [gRPC](https://grpc.io) server to acquire and store images from the inVADER ICCD. The server API and message formats are described in [protos/imagesvc.proto](https://bitbucket.org/uwaploe/imagerpc/src/master/protos/imagesvc.proto).
