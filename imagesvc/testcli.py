from . import imagetest
import argparse
import logging
import sys
import signal
import os.path


def main():
    parser = argparse.ArgumentParser(
        description="gRPC server to test image streaming")
    parser.add_argument("--addr", type=str,
                        metavar="HOST:PORT",
                        default="[::]:10131",
                        help="host:port on which server will listen")
    parser.add_argument("--debug", action="store_true",
                        help="enable debugging output")
    parser.add_argument("--dir", type=str,
                        default=os.path.expanduser("~/images"),
                        help="directory containing test images")
    args = parser.parse_args()

    if args.debug:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                        level=level,
                        stream=sys.stderr)

    svc = imagetest.create_server(args.addr, args.dir)
    svc.start()
    try:
        signal.pause()
    finally:
        logging.info("Waiting for server shutdown")
        event = svc.stop(grace=0)
        logging.info('Server stop: %s', event)
        event.wait()
        logging.info('Stopped')
