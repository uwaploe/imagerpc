from . import imagesvc
from ximea import xiapi
import argparse
import logging
import sys
import signal
import os.path


def main():
    parser = argparse.ArgumentParser(
        description="Image acquisition gRPC server")
    parser.add_argument("--addr", type=str,
                        metavar="HOST:PORT",
                        default="localhost:10130",
                        help="host:port on which server will listen")
    parser.add_argument("--debug", action="store_true",
                        help="enable debugging output")
    args = parser.parse_args()

    if args.debug:
        level = logging.DEBUG
    else:
        level = logging.INFO

    if os.environ.get("JOURNAL_STREAM") is not None:
        # No need for timestamp when running under Systemd
        logging.basicConfig(format='%(levelname)s %(message)s',
                            level=level,
                            stream=sys.stderr)
    else:
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                            level=level,
                            datefmt='%Y-%m-%d %H:%M:%S',
                            stream=sys.stderr)
    try:
        cam = xiapi.Camera()
        cam.open_device()
        cam.set_debug_level("XI_DL_FATAL")
    except Exception:
        logging.exception("Aborting ...")
        sys.exit(1)

    svc = imagesvc.create_server(cam, args.addr)
    svc.start()
    try:
        signal.pause()
    finally:
        cam.close_device()
        logging.info("Waiting for server shutdown")
        event = svc.stop(grace=0)
        logging.info('Server stop: %s', event)
        event.wait()
        logging.info('Stopped')
