#!/usr/bin/env python3

from concurrent import futures
from . import imagesvc_pb2 as api # type: ignore
from . import imagesvc_pb2_grpc as svc # type: ignore
import grpc # type: ignore
import threading
import os
import os.path
import logging
import random
from grpc_reflection.v1alpha import reflection # type: ignore


_LOGGER = logging.getLogger(__name__)
_LOGGER.addHandler(logging.NullHandler())


# Chunk size for image data transfer
_CHUNK_SIZE = 16*1024


def stream_image_file(dirname: str, size: int):
    """
    Choose a random image file from a directory and stream it in chunks of
    'size' bytes.
    """
    files = []
    with os.scandir(dirname) as listing:
        for entry in listing:
            if entry.is_file():
                files.append(os.path.join(dirname, entry.name))
    with open(random.choice(files), "rb") as f:
        while True:
            buf = f.read(size)
            if len(buf) == 0:
                break
            yield api.ImageData(data=buf)


class ImageTestServicer(svc.ImageTestServicer):
    """
    Implement a gRPC server to stream images from a directory.
    """
    def __init__(self, imgdir: str = "."):
        self.imgdir = imgdir
        self.mutex = threading.Lock()

    def GetImage(self, req, ctx):
        try:
            md = dict(ctx.invocation_metadata())
            chunk_size = int(md["image-chunksize"]) or _CHUNK_SIZE
        except Exception:
            chunk_size = _CHUNK_SIZE
        with self.mutex:
            _LOGGER.debug("GetImage(%s)", req)
            try:
                yield from stream_image_file(self.imgdir, chunk_size)
            except Exception as e:
                ctx.set_code(grpc.StatusCode.INTERNAL)
                ctx.set_details(str(e))
                ctx.cancel()
                _LOGGER.exception("Error")


def create_server(address: str, imgdir: str = "."):
    """
    Create a new ImageTest gRPC server to listen at the
    specified address.
    """
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=5))
    svc.add_ImageTestServicer_to_server(ImageTestServicer(imgdir), server)
    SERVICE_NAMES = (
        api.DESCRIPTOR.services_by_name['ImageTest'].full_name,
        reflection.SERVICE_NAME,
    )
    reflection.enable_server_reflection(SERVICE_NAMES, server)
    server.add_insecure_port(address)
    return server
