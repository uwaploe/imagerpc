# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

from . import imagesvc_pb2 as imagesvc__pb2


class ImageAcqStub(object):
    """Image acquisition service
    """

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.StartAcq = channel.unary_unary(
                '/imagesvc.ImageAcq/StartAcq',
                request_serializer=imagesvc__pb2.ImageSettings.SerializeToString,
                response_deserializer=imagesvc__pb2.Empty.FromString,
                )
        self.Acquire = channel.unary_stream(
                '/imagesvc.ImageAcq/Acquire',
                request_serializer=imagesvc__pb2.AcqRequest.SerializeToString,
                response_deserializer=imagesvc__pb2.ImageData.FromString,
                )
        self.SoftTrigger = channel.unary_unary(
                '/imagesvc.ImageAcq/SoftTrigger',
                request_serializer=imagesvc__pb2.Empty.SerializeToString,
                response_deserializer=imagesvc__pb2.Empty.FromString,
                )
        self.StopAcq = channel.unary_unary(
                '/imagesvc.ImageAcq/StopAcq',
                request_serializer=imagesvc__pb2.Empty.SerializeToString,
                response_deserializer=imagesvc__pb2.Empty.FromString,
                )


class ImageAcqServicer(object):
    """Image acquisition service
    """

    def StartAcq(self, request, context):
        """Configure the camera settings and enable image acquisition.
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def Acquire(self, request, context):
        """Acquire an image on the next trigger and stream the data to
        the client in TIFF format.
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def SoftTrigger(self, request, context):
        """Send a software trigger to the camera.
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def StopAcq(self, request, context):
        """Disable image acquisition mode
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_ImageAcqServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'StartAcq': grpc.unary_unary_rpc_method_handler(
                    servicer.StartAcq,
                    request_deserializer=imagesvc__pb2.ImageSettings.FromString,
                    response_serializer=imagesvc__pb2.Empty.SerializeToString,
            ),
            'Acquire': grpc.unary_stream_rpc_method_handler(
                    servicer.Acquire,
                    request_deserializer=imagesvc__pb2.AcqRequest.FromString,
                    response_serializer=imagesvc__pb2.ImageData.SerializeToString,
            ),
            'SoftTrigger': grpc.unary_unary_rpc_method_handler(
                    servicer.SoftTrigger,
                    request_deserializer=imagesvc__pb2.Empty.FromString,
                    response_serializer=imagesvc__pb2.Empty.SerializeToString,
            ),
            'StopAcq': grpc.unary_unary_rpc_method_handler(
                    servicer.StopAcq,
                    request_deserializer=imagesvc__pb2.Empty.FromString,
                    response_serializer=imagesvc__pb2.Empty.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'imagesvc.ImageAcq', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class ImageAcq(object):
    """Image acquisition service
    """

    @staticmethod
    def StartAcq(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/imagesvc.ImageAcq/StartAcq',
            imagesvc__pb2.ImageSettings.SerializeToString,
            imagesvc__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def Acquire(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_stream(request, target, '/imagesvc.ImageAcq/Acquire',
            imagesvc__pb2.AcqRequest.SerializeToString,
            imagesvc__pb2.ImageData.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def SoftTrigger(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/imagesvc.ImageAcq/SoftTrigger',
            imagesvc__pb2.Empty.SerializeToString,
            imagesvc__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def StopAcq(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/imagesvc.ImageAcq/StopAcq',
            imagesvc__pb2.Empty.SerializeToString,
            imagesvc__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)


class ImageTestStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.GetImage = channel.unary_stream(
                '/imagesvc.ImageTest/GetImage',
                request_serializer=imagesvc__pb2.Empty.SerializeToString,
                response_deserializer=imagesvc__pb2.ImageData.FromString,
                )


class ImageTestServicer(object):
    """Missing associated documentation comment in .proto file."""

    def GetImage(self, request, context):
        """Send a random test image
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_ImageTestServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'GetImage': grpc.unary_stream_rpc_method_handler(
                    servicer.GetImage,
                    request_deserializer=imagesvc__pb2.Empty.FromString,
                    response_serializer=imagesvc__pb2.ImageData.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'imagesvc.ImageTest', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class ImageTest(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def GetImage(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_stream(request, target, '/imagesvc.ImageTest/GetImage',
            imagesvc__pb2.Empty.SerializeToString,
            imagesvc__pb2.ImageData.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
