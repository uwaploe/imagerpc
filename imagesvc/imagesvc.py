#!/usr/bin/env python3

from ximea import xiapi
from datetime import datetime, timezone
import tifffile as tf
from concurrent import futures
from . import imagesvc_pb2 as api
from . import imagesvc_pb2_grpc as svc
import grpc
import threading
import os
import os.path
import logging
import io
from grpc_reflection.v1alpha import reflection


_LOGGER = logging.getLogger(__name__)
_LOGGER.addHandler(logging.NullHandler())


# Chunk size for image data transfer
_CHUNK_SIZE = 16*1024


def stream_image(img: xiapi.Image, ts: datetime, size: int):
    """
    Convert an XIMEA camera image to grayscale TIFF and stream it in chunks
    of 'size' bytes.
    """
    # Save image data in TIFF format to an in-memory buffer
    buf = io.BytesIO()
    data = img.get_image_data_numpy()
    tw = tf.TiffWriter(buf)
    tw.save(data, compress=1, photometric='minisblack',
            datetime=ts.strftime('%Y:%m:%d %H:%M:%S'),
            resolution=(1200, 1200, 'inch'))
    imagedata = buf.getvalue()
    n = len(imagedata)
    _LOGGER.debug("Image size: %d bytes", n)
    # Stream it ...
    i = 0
    while i < n:
        j = i+size
        if j > n:
            j = n
        yield api.ImageData(data=imagedata[i:j])
        i += size


class ImageAcqServicer(svc.ImageAcqServicer):
    """
    Implement a gRPC server to acquire images from the inVADER ICCD via the
    XIMEA camera API.
    """
    def __init__(self, cam: xiapi.Camera):
        self.mutex = threading.Lock()
        self.cam = cam
        self.img = xiapi.Image()
        self.is_ready = False

    def StartAcq(self, req, ctx):
        with self.mutex:
            _LOGGER.debug("StartAcq(%r)", req)
            try:
                self.cam.set_imgdataformat(req.format or "XI_MONO16")
                self.cam.set_sensor_bit_depth("XI_BPP_14")
                self.cam.set_output_bit_depth("XI_BPP_14")
                self.cam.set_exposure(req.exposure)
                if req.gain >= 0:
                    self.cam.set_gain(req.gain)
                if req.trigger == api.ImageSettings.TriggerSource.HARDWARE:
                    self.cam.set_trigger_source("XI_TRG_EDGE_FALLING")
                else:
                    self.cam.set_trigger_source("XI_TRG_SOFTWARE")
                self.cam.start_acquisition()
                self.is_ready = True
            except xiapi.Xi_error as err:
                ctx.set_code(grpc.StatusCode.INTERNAL)
                ctx.set_details(str(err))
                ctx.cancel()
                _LOGGER.exception("xiAPI error")
        return api.Empty()

    def Acquire(self, req, ctx):
        try:
            md = dict(ctx.invocation_metadata())
            chunk_size = int(md["image-chunksize"]) or _CHUNK_SIZE
        except Exception:
            chunk_size = _CHUNK_SIZE

        with self.mutex:
            _LOGGER.debug("Acquire(%r)", req)
            if not self.is_ready:
                ctx.set_code(grpc.StatusCode.UNAVAILABLE)
                ctx.set_details("StartAcq has not been called")
                ctx.cancel()
                _LOGGER.error("StartAcq has not been called")
                return
            try:
                self.cam.get_image(self.img, timeout=req.timeout)
                ts = datetime.now(timezone.utc)
                yield from stream_image(self.img, ts, chunk_size)
            except xiapi.Xi_error as err:
                ctx.set_code(grpc.StatusCode.INTERNAL)
                ctx.set_details(str(err))
                ctx.cancel()
                _LOGGER.exception("xiAPI error")

    def StopAcq(self, req, ctx):
        with self.mutex:
            _LOGGER.debug("StopAcq()")
            try:
                self.cam.stop_acquisition()
            except xiapi.Xi_error as err:
                ctx.set_code(grpc.StatusCode.INTERNAL)
                ctx.set_details(str(err))
                ctx.cancel()
                _LOGGER.exception("xiAPI error")
            self.is_ready = False
            return api.Empty()

    def SoftTrigger(self, req, ctx):
        _LOGGER.debug("SoftTrigger()")
        self.cam.set_trigger_software(1)
        return api.Empty()


def create_server(cam: xiapi.Camera, address: str):
    """
    Create a new ImageAcq gRPC server to listen at the
    specified address.
    """
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=5))
    svc.add_ImageAcqServicer_to_server(ImageAcqServicer(cam), server)
    SERVICE_NAMES = (
        api.DESCRIPTOR.services_by_name['ImageAcq'].full_name,
        reflection.SERVICE_NAME,
    )
    reflection.enable_server_reflection(SERVICE_NAMES, server)
    server.add_insecure_port(address)
    return server
