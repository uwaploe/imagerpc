#!/usr/bin/env python3

from imagesvc import imagesvc
import datetime
from unittest.mock import Mock

def test_file_naming():
    table = [
             {
                 "dir": "/data",
                 "tmpl": "%Y/%j/{}_%Y%m%d_%H%M%S.tif",
                 "expect": "/data/2019/001/test_20190101_000000.tif"
             },
             {
                 "dir": "/data",
                 "tmpl": "{}/%Y/%j/%Y%m%d_%H%M%S.tif",
                 "expect": "/data/test/2019/001/20190101_000000.tif"
             }
            ]
    obj = imagesvc.ImageAcqServicer(Mock())
    ts = datetime.datetime(2019, 1, 1, tzinfo=datetime.timezone.utc)

    for entry in table:
        req = Mock(dir=entry["dir"], template=entry["tmpl"])
        obj.SetStorage(req, None)
        name = obj.gen_pathname(ts, "test")
        assert name == entry["expect"]
